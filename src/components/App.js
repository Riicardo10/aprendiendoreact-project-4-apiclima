import React, { Component } from 'react';
import Header               from './Header';
import Formulario           from './Formulario';
import Clima                from './Clima';
import Error                from './Error';

class App extends Component {

  state = {
    error: '',
    consulta: {},
    ciudad_404: false
  }

  componentDidMount() {
    this.setState( {error: false} );
  }

  componentDidUpdate( prevProps, prevState ) {
    if( prevState.consulta !== this.state.consulta ) {
      this.consultarAPI();
    }
  }

  datosConsulta = (data) => {
    if( data.ciudad === '' || data.pais === '' ) {
      this.setState( {error: true} );
    }
    else{
      this.setState( {
        consulta: data,
        error: false
      } );
    }
  }

  consultarAPI = () => {
    const {ciudad, pais} = this.state.consulta;
    if( !ciudad || !pais || pais == 0) {
      return null
    };
    const KEY = 'b6907d289e10d714a6e88b30761fae22';
    let   URL = 'https://openweathermap.org/data/2.5/weather?q=' + ciudad + ',' + pais + '&appid=' + KEY;
    fetch( URL )
      .then( res => {
        return res.json();
      } )
      .then( datos => {
        this.setState( {ciudad_404: false} );
        this.setState( {consulta: datos} );
      } )
      .catch( err => {
        this.setState( {ciudad_404: true} );
        console.log( err );
      } );
  }

  mostrarComponente = () => {
    if( this.state.error ) {
      return <Error mensaje='Ambos campos son obligatorios' />
    }
    else if( this.state.ciudad_404 ) {
      return <Error mensaje='Ciudad no encontrada' />
    }
    else {
      return <Clima consulta={this.state.consulta} ciudad={this.state.ciudad_404}/>
    }
  }

  render() {
    return (
      <div className="app">
        <Header titulo='Clima' />
        <Formulario 
            datosConsulta={this.datosConsulta}/>
        { this.mostrarComponente() }
        {/* {this.state.error ? <Error mensaje='Ambos campos son obligatorios' /> : <Clima consulta={this.state.consulta} ciudad={this.state.ciudad_404}/>} */}
      </div>
    );
  }
}

export default App;
