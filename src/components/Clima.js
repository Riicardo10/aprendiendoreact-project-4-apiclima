import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Clima extends Component {

    mostrarDatos = () => {
        const {name, weather, main} = this.props.consulta;
        if( !name || !weather || !main ) return null;
        const url_icon = 'http://openweathermap.org/img/w/' + weather[0].icon + '.png';
        const alt_icon = 'Clima de: ' + name;
        return (
            <div className='row' >
                <div className='resultado col s12 m8 l6 offset-m2 offset-l3' >
                    <div className='card-panel light-blue align-center' >
                        <span className='white-text'>
                            <h2>Clima de : {name}</h2>
                            <p className='temperatura'>
                                Actual: { (main.temp ).toFixed(2) } &deg;C
                                <img src={url_icon} alt={alt_icon} title={alt_icon} />
                            </p>
                            <p> Max. {main.temp_max} &deg;C</p>
                            <p> Max. {main.temp_min} &deg;C</p>
                        </span>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return (
            <div className='container'>
                {this.mostrarDatos()}    
            </div>
        );
    }
}

Clima.propTypes = {
    consulta: PropTypes.object.isRequired
}

export default Clima;